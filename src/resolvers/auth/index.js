const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = {
  Mutation: {
    login: async (_, { data }, { models }) => {
      try {
        if (!data.phone) throw new Error("Phone number can't be null")
        if (!data.password) throw new Error("Password can't be null")

        const checkUser = await models.userModel.findOne({ phone: data.phone }).exec();
        if (!checkUser) throw new Error("Invalid phone number or password")

        const checkPassword = await bcrypt.compare(data.password, checkUser.password)
        if (!checkPassword) throw new Error("Invalid phone number or password")

        const accessToken = jwt.sign({ id: checkUser?._id }, process.env.TOKEN_KEY, { expiresIn: "1d" });
        const refreshToken = jwt.sign({ id: checkUser?._id }, process.env.REFRESH_TOKEN_KEY, { expiresIn: "7d" });

        const resData = { accessToken: accessToken, refreshToken: refreshToken, data: checkUser }
        return resData
      } catch (err) {
        throw new Error(err);
      }
    },

    register: async (_, { data }, { models }) => {
      try {
        if (!data.phone) throw new Error("Phone number can't be null")
        if (!data.password) throw new Error("Password can't be null")

        const hashPassword = await bcrypt.hash(data.password, 10)
        const createUser = await models.userModel.create({ ...data, password: hashPassword });

        const accessToken = jwt.sign({ id: createUser?._id }, process.env.TOKEN_KEY, { expiresIn: "1d" });
        const refreshToken = jwt.sign({ id: createUser?._id }, process.env.REFRESH_TOKEN_KEY, { expiresIn: "7d" });

        const resData = { accessToken: accessToken, refreshToken: refreshToken, data: createUser }
        return resData
      } catch (err) {
        throw new Error(err);
      }
    },
  },
};
