const { makeExecutableSchema } = require("@graphql-tools/schema");
const { ApolloServer } = require("apollo-server-express");
const { createServer } = require("http");
const mongoose = require("mongoose");
const express = require("express");
const cors = require("cors");
require("dotenv").config();

const models = require("./models");
const resolvers = require("./resolvers");
const schemas = require("./schemas");

const startApolloServer = async () => {
  mongoose.connect(process.env.MONGO_URL, { useUnifiedTopology: true, useNewUrlParser: true });
  
  const typeDefs = schemas;
  const schema = makeExecutableSchema({ typeDefs, resolvers });

  const server = new ApolloServer({
    schema,
    context: async ({ req }) => {
      return {
        req,
        models,
      };
    },
  });

  await server.start();
  const app = express();
  app.use(cors());

  const httpServer = createServer(app);

  server.applyMiddleware({ app, path: "/" });
  await new Promise((resolve) => httpServer.listen({ port: process.env.PORT }, resolve));

  console.log(`🚀 Server ready at http://localhost:${process.env.PORT}`);

  return { server, app };
};

startApolloServer();
