var mongoose = require("mongoose");

var _userSchema = mongoose.Schema({
    firstName: String,
    lastName: String,
    phone: String,
    password: String,
    createdAt: {
        type: Date,
        default: Date.now,
    },
    updatedAt: {
        type: Date,
        default: Date.now,
    },
    note: {
        type: String,
        default: "User Created for JaoCanDev 2"
    }
});

var userModel = mongoose.model("user", _userSchema, "User");
module.exports = userModel;