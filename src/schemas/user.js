const { gql } = require("apollo-server");
module.exports = gql`
    type User {
        id: ID
        firstName: String
        lastName: String
        phone: String
        createdAt: DateTime
        updatedAt: DateTime
        note: String
    }

    input UserInput {
        firstName: String
        lastName: String
        phone: String
        password: String
        note: String
    }

    input UserWhereInput {
        firstName: String
        lastName: String
        phone: String
        password: String
        createdAt_gte: DateTime
        createdAt_lt: DateTime
        note: String
    }

    input UserWhereInputOne {
        id: ID
    }

    type UserResponse {
        total: Int
        data: [User]
    }

    extend type Query {
        user(where: UserWhereInputOne!): User
        users(where: UserWhereInput, orderBy: OrderByInput, skip: Int, limit: Int): UserResponse
    }

    extend type Mutation {
        createUser(data: UserInput!): User
        updateUser(data: UserInput!, where: UserWhereInputOne!): User
        deleteUser(where: UserWhereInputOne!): User
    }
`;