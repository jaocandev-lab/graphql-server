const { gql } = require("apollo-server");
module.exports = gql`
    input UserAuthInput {
        phone: String
        password: String
    }

    type UserAuthResponse {
        accessToken: String
        refreshToken: String
        data: User
    }

    extend type Mutation {
        login(data: UserAuthInput!): UserAuthResponse
        register(data: UserInput!): UserAuthResponse
    }
`;