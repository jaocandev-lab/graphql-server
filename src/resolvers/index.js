const userResolver = require("./user");
const userAuthResolver = require("./auth");

module.exports = [
    userResolver,
    userAuthResolver
];