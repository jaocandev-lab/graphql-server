const bcrypt = require("bcrypt");
const { queryWithWhere, queryWithOrderBy } = require("./helper");

module.exports = {
  Query: {
    user: async (_, { where }, { models }) => {
      try {
        const user = await models.userModel.findById(where.id).exec();
        if (!user) throw new Error("User not found")

        return user
      } catch (err) {
        throw new Error(err)
      }
    },

    users: async (_, { where, orderBy, skip, limit }, { models }) => {
      try {
        const _where = queryWithWhere(where);
        const _orderBy = queryWithOrderBy(orderBy);

        const countUsers = await models.userModel.count(_where).exec();
        const users = await models.userModel.find(_where).sort(_orderBy).skip(skip || 0).limit(limit || 50).exec();

        const response = { total: countUsers, data: users }
        return response
      } catch (err) {
        throw new Error(err)
      }
    },
  },

  Mutation: {
    createUser: async (_, { data }, { models }) => {
      try {
        if (!data.phone) throw new Error("Phone number can't be null")
        if (!data.password) throw new Error("Password can't be null")

        const hashPassword = await bcrypt.hash(data.password, 10)
        const createUser = await models.userModel.create({ ...data, password: hashPassword });

        return createUser;
      } catch (err) {
        throw new Error(err)
      }
    },

    updateUser: async (_, { data, where }, { models }) => {
      try {
        const updateUser = await models.userModel.findByIdAndUpdate(where.id, data).exec();

        return updateUser;
      } catch (err) {
        throw new Error(err)
      }
    },

    deleteUser: async (_, { where }, { models }) => {
      try {
        const deleteUser = await models.userModel.findByIdAndDelete(where.id).exec();
        
        return deleteUser;
      } catch (err) {
        throw new Error(err)
      }
    },
  },
};
